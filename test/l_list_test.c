//This is the driver program that acts as a 
//preliminary test for the l_list implementation


#include <l_list.h>

int 
main(void) {
    int ret_val = 1;
    l_list* list = create_list();
    if (list == NULL) {
        fprintf(stderr, "Couldn't create list.\n");
        exit(ENOMEM);
    }

    add_node(list, 5854510279, "Christopher Naugle\0");
    add_node(list, 5854510410, "Sandy Naugle");
    add_node(list, 4437570539, "Christoper's Work phone number");
    add_node(list, 5855899555, "Mom and Dad Hales' home phone number");
    add_node(list, 5709163951, "Mom Naugle's cell phone");
    add_node(list, 3016778800, "Kimbrough Ambulatory Care Center");
    add_node(list, 4437570528, "Deer Path main phone line");
    
    if (!ret_val) {
        fprintf(stderr, "Couldn't add node.\n");
        exit(ENOMEM);
    }

    //TESTS FOLLOW
    print_list(list);
    node* node = find_node(list, 4437570528);
    printf("*************************************\n");
    if (node != NULL) {
        printf("Value: %s\n", node->data);
    } else {
        printf("Didn't find it!!!\n");
    }
    bool deleted = delete_node(list, 5854510279);
    if (!deleted) {
        printf("Couldn't delete node!!!\n");
    } else {
        printf("*****************************\n");
        print_list(list);
    }
    destroy_list(list);

    return 0;
}
