//This is a driver program that tests the hash_table implementation

#include <h_table.h>


/*int main(int argc, char** argv) {
    if (argc != 2) {
        fprintf(stderr, "USAGE: %s <input number>\n", argv[0]);
        return 0;
    }
   
    long int input = strtol(argv[1], NULL, 10);
    if (input > (long int)sqrt(LONG_MAX)) {
        fprintf(stderr, "Input number is too big\n");
        return 0;
    }

    printf("Putting %ld in index: %lu\n", input, hash(input));

    return 0;
}*/

int main(void) {
    l_list** table = create_hash_table();
    for (int i = 0; i < 10000; i++) {
        add_to_table(table, i, "asqwqeasdsdlkfsdlkfj");
    }

    print_hash_table(table);
    delete_hash_table(table);
}
        
