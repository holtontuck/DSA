//This is the driver/test program for
//the binary tree implementation(b_tree)


#include <b_tree.h>


int main(void) {
    //int array[] = {10,5,7,1,2,15,20,14,25,18};
    //int array[] = {10,5,7,1,27,15,20,14,25,18};
    //int array[] = {10,5,2,1,3,15,20,14,25,18};
    int array[] = {10,5,2,1,3,6,15,20,14,25,18};

    B_tree* tree = create_bTree();
    if (tree == NULL) {
        fprintf(stderr, "Couldn't allocate tree. Memory error.\n");
        return 1;
    }
    for (int i = 0; i < 11; i++) {
        add_node(tree, array[i]);
    }
    printf("**************************************************\n");
    In_order(tree->root);
    delete_node(tree, 15);
    printf("**************************************************\n");
    In_order(tree->root);
    //delete_node(tree, 20);
    //printf("**************************************************\n");
    //In_order(tree->root);
    delete_node(tree, 2);
    printf("**************************************************\n");
    In_order(tree->root);
    delete_node(tree, 10);
    printf("**************************************************:\n");
    In_order(tree->root);
    return 0;
}
