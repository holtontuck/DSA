.PHONY: clean clean-test


TESTS=l_list_test hash_table_test bTree_test

INC=-I/home/christopher/DSA/include

all: l_list.o dl_list.o cl_list.o h_table.o b_tree.o 
	
#b_tree: b_tree.o


test: $(TESTS)

l_list_test: 
	cp src/l_list.c .
	cp test/l_list_test.c .
	cc l_list.c l_list_test.c -o l_list_test $(INC)
	rm l_list.c l_list_test.c

hash_table_test:
	cp src/l_list.c .
	cp src/h_table.c .
	cp test/hash_table_test.c .
	cc l_list.c h_table.c hash_table_test.c -o hash_table_test $(INC) -lm -g
	rm l_list.c h_table.c hash_table_test.c 

bTree_test:
	cp src/b_tree.c .
	cp test/bTree_test.c .
	cc b_tree.c bTree_test.c -o bTree_test $(INC) -g
	rm b_tree.c bTree_test.c          
	
//l_list.o:
#	cp src/l_list.c .
#	cc l_list.c -o l_list $(INC)
#	rm l_list.c


dl_list.o:
	cp src/dl_list.c .
	cc dl_list.c -o dl_list $(INC)
	rm dl_list.c

cl_list.o:
	cp src/cl_list.c .
	cc cl_list.c -o cl_list $(INC)
	rm cl_list.c

h_table.o:
	cp src/h_table.c .
	cc h_table.c -o h_table $(INC) -lm
	rm h_table.c

b_tree:
	cp src/b_tree.c .
	cc b_tree.c -o b_tree $(INC) -g
	rm b_tree.c




clean:
	rm -rf l_list dl_list cl_list h_table b_tree

clean-test:
	rm -rf $(TESTS) 
