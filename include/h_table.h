#ifndef H_TABLE_H
#define H_TABLE_H


#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <l_list.h>

//Simple function that returns the square of an integer
long int
square(long int num);


//Function that computes how many digits a number contains
uint8_t
num_digits(long int number);


//The actual hash function
//Takes an int as input, 
//and returns the index where the input should be placed
long int hash (long int input);


//The hashtable will be a dynamically allocated array where each index corresponds 
//to a linked list of "buckets" to hold the input integers
l_list** create_hash_table();


//Prints the hash table
void
print_hash_table(l_list** hash_array);


//Deletes the hash table and frees resources    
void
delete_hash_table(l_list** hash_array);


//Hash the input value and add it to the table in the appropriate place
bool
add_to_table(l_list** hash_array, long int key, char* data);


#endif
