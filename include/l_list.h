#ifndef L_LIST_H
#define L_LIST_H

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>


#define MAX_STRING  255
// Singly linked list should have the following components:
//1. Structure representing the list itself
//2. Structure representing a node in the list
//
//Functions needed
//1. Create/initialize a linked list
//2. Create a node
//3. Delete a node
//4. Destroy a linked list
//5. Get the value in a node
//6. Set the value in a node


//Basic structures follow
typedef struct node {
    long int key;
    char* data;
    struct node* next;
} node;

typedef struct l_list {
    node* head;
    size_t size;
} l_list;


//This function creates a linked list.
l_list* create_list();


//This function will add a node to a list.
bool
add_node(l_list* list, long int key, char* value);


//This function will print the data for a given list, if the list exists.
void
print_list(l_list* list);


//This function will delete the node whose data matches the input data, if
//found, and return true on success. Otherwise, return false.
bool
delete_node(l_list* list, long int key);


//This function will return the address of the node, if the node's data matches
//the given input data
node*
find_node(l_list* list, long int key);


/* This function will delete all the nodes but leave the list intact */
bool 
clear_list(l_list* list);


/*This function will delete all nodes and the list itself,
    and free all dynamically allocated memory */
void 
destroy_list(l_list* list);


#endif
