#ifndef B_TREE_H
#define B_TREE_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



struct node {
    struct node*    parent;
    struct node*    left;
    struct node*    right;
    int             data;
};

typedef struct node Node;

struct b_tree {
    Node*           root;
    int             size;
};

typedef struct b_tree B_tree;


//This function creates a new B_tree and returns a pointer to it
B_tree*
create_bTree();


//This function will add a child node to the tree.
//if input value < parent, child will be left node.
//Otherwise, child will be right node.
bool
add_node(B_tree* tree, int data);


//This will find and return an empty node 
//Where the input data can be stored
Node* 
find_empty_node(B_tree* tree, int data);


//This will find and return a node whose data matches
//the input data
Node* 
find_node(B_tree* tree, int data);


//This function will, given an input, find and delete the node
//with that input value in the data field.
bool
delete_node(B_tree* tree, int data);


//This function came from Noel Kalicharan's book: Data Structures in C
//This visits a particular node
void visit(Node* node);

//This function also came from Noel Kalicharan's book: Data Strctures in C
//This will print out an in-order travseral of a binary tree
//whose pointer is passed to it.
void
In_order(Node* node);

#endif
