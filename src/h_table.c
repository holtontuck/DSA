/*This code is an implementation of a hashtable.
 * The hashing function is based upon mid-square hashing. 
 *I will use a "buckets" implementation. That is, an
 *array with a linked list attached to each index*/


#include <h_table.h>

#define ARRAY_SIZE  100
#define MAX_DIGITS  19// The most digits a long integer can contain
#define NUM_MID_DIGITS ARRAYSIZE - 1


//The hashtable will be a dynamically allocated array where each index corresponds 
//to a linked list of "buckets" to hold the input integers
l_list** create_hash_table(){
    l_list** hash_array = calloc(1, sizeof(l_list*) * ARRAY_SIZE);
    if (hash_array == NULL) {
        return NULL;
    }
    return hash_array;
}

//Prints the hash table
void
print_hash_table(l_list** hash_array) {
    if (hash_array == NULL) {
        fprintf(stderr, "invalid hash_array\n");
        exit(1);
    }

    for (int i = 0; i < ARRAY_SIZE; i++) {
        
        l_list* list = *(hash_array + i);
        if (list != NULL) {
            print_list((l_list*)list);
        }
    }
}


//Deletes the hash table and frees resources    
void
delete_hash_table(l_list** hash_array) {
    for (int i = 0; i < ARRAY_SIZE; i++) {
        
        l_list* list = *(hash_array + i);
        if (list != NULL) {
            destroy_list(list);
        }
    }
    free(hash_array);
}


//Hash the input value and add it to the table in the appropriate place
bool
add_to_table(l_list** hash_array, long int key, char* data) {
    long int idx = hash(key);
    bool success = false;
    if (*(hash_array + idx) == NULL) {
        l_list* list = create_list();
        if (list == NULL) {
            fprintf(stderr, "Memory Error. Couldn't create linked list");
            return false;
        }
        *(hash_array + idx)  = list;
        success = add_node(list, key, data);
        if (!success) {
            fprintf(stderr, "Memory Error. Couldn't add node");
            free(list);
            return false;
        }
    } else {
        success = add_node(*(hash_array + idx), key, data);
        if (!success) {
            fprintf(stderr, "Memory Error. Couldn't add node");
            return false;
        }
    }
}
        

//Simple function that returns the square of an integer
long int
square(long int num) {
    return num * num;
}


//Function that computes how many digits a number contains
uint8_t
num_digits(long int number) {
    //account for even powers of 10 by adding 1
    //Otherwise the number of digits return is one too few
    uint8_t add = 0;
    float first_log = log10(number);
    float second_log =  ceil(log10(number));
    if (first_log == second_log) {
        add = 1;
    }
    return (uint8_t)ceil(log10(number) + add);
}


//The actual hash function
//Takes an int as input, 
//and returns the index where the input should be placed
long int
hash (long int input) {
    if (input > LONG_MAX) {
        return -1;
    }
    //first take an input long integer
    //Next, square the integer
    long int squared = square(input);
    //How many digits are in it? Find the middle digit(s)
    uint8_t num_digits_total = num_digits(squared);
    uint8_t num_middle_digits = num_digits(ARRAY_SIZE) - 1;
    uint32_t starting_idx = num_digits_total / 2;     

    char squared_string[MAX_DIGITS + 1] = "";
    //Convert the squared number to a string representation
    snprintf(squared_string, MAX_DIGITS, "%ld", squared);
   
    //Now convert the extracted middle numbers back to an int
    //representing the index in which to put the given input 
    char extracted_num[num_middle_digits];
    memset(extracted_num, 0, num_middle_digits + 1);
    memcpy(extracted_num, squared_string + starting_idx, num_middle_digits);
    long unsigned int index = strtol(extracted_num, NULL, 10) % ARRAY_SIZE;
            
    return index;
}


//ADDING DATA TO HASH_TABLE
// 1) define an array of pointers, of size[ARRAY_SIZE]
//      and initialize all elements to zero

// 2) take a given input and hash it. If the index returned contains null
//      create a linked list, store the linked list's pointer 
//      in the selected index. Create/add a node containing the input value
//      to the newly created linked list.

// 3) If the index returned from the hasing function contains a pointer to
//      a linked list, fetch the address of the linked list and pass that
//      list and the input data to the add_node function. 


//RETRIEVING DATA FROM TABLE
//TODO
