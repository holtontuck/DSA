#include <b_tree.h>

void
In_order(Node* node);


//This function creates a new B_tree and returns a pointer to it
B_tree*
create_bTree() {
    B_tree* tree = calloc(1, sizeof(*tree));
    if (tree == NULL) {
        return NULL;
    }
    tree->root =            NULL;
    tree->size =            0;
    return tree;
}


//This function will add a child node to the tree.
//if input value < parent, child will be left node.
//Otherwise, child will be right node.
bool
add_node(B_tree* tree, int data) {
    if (tree == NULL) {
        return false;
    }
    //First case, root is NULL.
    if (tree->root == NULL) {
        tree->root = calloc(1, sizeof(*tree->root));
        if (tree->root == NULL) {
            return false;
        }
        tree->root->data = data;
        printf("Adding %d at root\n", tree->root->data);
        return true;
    }
    Node* current = tree->root;
    while(current) {
        if (data < current->data) {
            if (current->left) {
                current = current->left;
            } else {
                printf("Adding %d left!!!\n", data);
                current->left = calloc(1, sizeof(*current->left));
                if (current->left == NULL) {
                    return false;
                }
                current->left->data = data;
                current->left->parent = current;
                printf("Parent value is %d\n", current->data); 
                return true;
            };
                      
        } else {
            if (current->right) {
                current = current->right;
            } else {
                printf("Adding %d right!!!\n", data);
                current->right = calloc(1, sizeof(*current->left));
                if (current->right == NULL) {
                    return false;
                }
                current->right->data = data;
                current->right->parent = current;
                printf("Parent value is %d\n", current->data); 
                return true;
            };
        }
    }
}


//This will find and return an empty node 
//Where the input data can be stored
Node* 
find_empty_node(B_tree* tree, int data) {
     Node* current = tree->root;
    while(current) {
        if (data < current->data) {
            if (current->left) {
                current = current->left;
            } else {
                return current; 
            };
                      
        } else {
            if (current->right) {
                current = current->right;
            } else {
                return current;
            };
        }
    }
    return NULL;
}


//This will find and return a node whose data matches
//the input data
Node* 
find_node(B_tree* tree, int data) {
     Node* current = tree->root;
    while(current) {
        if (data == current->data) {
                return current;
        } else if (data <= current->data) {
                current = current->left;
        } else if (data > current->data) {
                current = current->right;
        }
    }
    return NULL;
}


//This function will, given an input, find and delete the node
//with that input value in the data field.
bool
delete_node(B_tree* tree, int data) {
    printf("DELETING: %d\n", data);
    if (tree == NULL) {
        return false;
    }
    Node* temp = tree->root;
    if (tree->root->data == data) {
        //If left node can be promoted to root
        if (tree->root->left != NULL) {
            //Promote left node to root
            tree->root = tree->root->left;
            tree->root->parent = NULL;
            //If the old root (now temp) has no right node, we're done
            if (temp->right == NULL) {
                free(temp);
                return true;
            //If the new root doesn't have a right node,
            //simply point root->right = temp->right
            }
            if (tree->root->right == NULL) {
                //establish the new parental relationship 
                temp->right->parent = tree->root;
                tree->root->right = temp->right;
                free(temp);
                return true;
            }
            else {
                //If there is already a right node coming from the new root,
                //find a place to attach the old root's right node
                Node* attach_node = find_empty_node(tree, temp->right->data);
                temp->right->parent = attach_node;
                attach_node->right = temp->right;
                free(temp);
                return true;
            }
        } else if (tree->root->right != NULL) {
            //Promote right node to root
            tree->root = tree->root->right;
            tree->root->parent = NULL;
            //If the old root (now temp) has no left node, we're done
            if (temp->left == NULL) {
                free(temp);
                return true;
            }
            //If the new root doesn't have a left node,
            //simply point root->left = temp->left
            else if (tree->root->left == NULL) {
                //establish the new parental relationship 
                temp->left->parent = tree->root;
                tree->root->left = temp->left;
                free(temp);
                return true;
            } else {
                //If there is already a left node coming from the new root,
                //find a place to attach the old root's left node
                Node* attach_node = find_empty_node(tree, temp->left->data);
                temp->left->parent = attach_node;
                attach_node->left = temp->left;
                free(temp);
                return true;
            }
            return false;
        }
    }
    //If the node to be deleted is not the root:
    //1. search the tree for the node pointer whose data matches the input data
    //2. call it temp.
    temp = find_node(tree, data);
    if (temp == NULL) {
        return false;
    }
    //Check to see if the node to delete is the right node
    if (temp->parent->right != NULL) {
        if (temp->parent->right == temp) {
            printf("RIGHT!!!\n");
            //Check to see if there is a right node to re-attach
            //If so, attach it; it not, set it to NULL;
            if (temp->right != NULL) {
                temp->parent->right = temp->right;
                temp->right->parent = temp->parent;
            } else temp->parent->right = NULL;
            
            //Check to see if there is a left node to re-attach
            if(temp->left != NULL) {
                Node* empty_node_parent = find_empty_node(tree, temp->left->data);
                //Make sure it didn't return itself as a node
                if (empty_node_parent != temp->left) {
                    if (empty_node_parent->data < temp->left->data) {
                        empty_node_parent->right = temp->left;
                    } else {
                        empty_node_parent->left = temp->left;
                    }
                    temp->left->parent = empty_node_parent;
                    free(temp);
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
    if (temp->parent->left != NULL) {
        if (temp->parent->left == temp) {
            printf("LEFT!!!\n");
            //Check to see if there is a left node to re-attach
            //If so, attach it; it not, set it to NULL;
            if (temp->left != NULL) {
                temp->parent->left = temp->left;
                temp->left->parent = temp->parent;
            } else temp->parent->left = NULL;
            
            //Check to see if there is a right node to re-attach
            if(temp->right != NULL) {
                printf("LOOKING FOR A PLACE FOR ORPHANED NODE\n");
                Node* new_parent = find_empty_node(tree, temp->right->data);
                printf("NEW PARENT NODE: %d\n", new_parent->data);
                if (new_parent->data < temp->right->data) {
                    new_parent->right = temp->right;
                } else {
                    new_parent->left = temp->right;
                }
                temp->right->parent = new_parent;
                free(temp);
                return true;
            } else {
                return false;
            }
        }
    }
    return false;
}


//This function came from Noel Kalicharan's book: Data Structures in C
void visit(Node* node) {
    printf("%d: ", node->data);
}


//This function also came from Noel Kalicharan's book: Data Strctures in C
//This will print out an in-order travseral of a binary tree
//whose pointer is passed to it.
void
In_order(Node* node) {
    if (node == NULL) {
        return;
    }
    In_order(node->left);
    visit(node);
    if (node->parent) {
        printf("Parent: %d\n", node->parent->data);
    }
    printf("\n");
    In_order(node->right);
}
