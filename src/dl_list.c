#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

// Doubly linked list should have the following components:
//1. Structure representing the list itself
//2. Structure representing a node in the list
//3. integer that tracks the size (how many nodes) of the list
//
//Functions needed
//1. Create/initialize a linked list
//2. Create a node
//3. Delete a node
//4. Destroy a linked list
//5. Get the value in a node


//Basic structures follow
typedef struct node {
    void* data;
    struct node* prev;
    struct node* next;
} node;

typedef struct dl_list {
    node* head;
    node* tail;
    size_t size;
} dl_list;


//Functions Follow


//This function creates a linked list.
dl_list* create_list() {
    dl_list* list = NULL;
    if ((list = malloc(1 * sizeof(*list))) == NULL) {
        return NULL;
    }
    list->head = NULL;
    list->tail = NULL;
    list->size = 0;
    return list;
}


//This function will add a node to a list.
bool
add_node(dl_list* list, void* data) {
    node* node = NULL;
    if ((node = malloc(1 * sizeof(*node))) == NULL) {
        return NULL;
    }
    node->data = data;
    if (list->head == NULL) {
        list->head = node;
        list->head->prev = NULL;
        list->tail = node;
        list->tail->next = NULL;
        list->size++;
        return true;
    }
    node->next = list->head;
    node->prev = NULL;
    list->head->prev = node;
    list->head = node;
    list->size++;
    return true;
}


//This function will print the data for a given list, if the list exists, from
//the head to the tail.
void
print_list_forward(dl_list* list) {
    if (!list || (list->size == 0)) {
        return;
    }
    node* current = list->head;
    node* temp = NULL;
    printf("List has %zu nodes\n", list->size);
    while(current) {
        printf("%s\n", (char*)current->data);
        current = current->next;
    }
}


//This function will print the data for a given list, if the list exists, from
//the tail to the head (in reverse).
void
print_list_reverse(dl_list* list) {
    if (!list || (list->size == 0)) {
      return;
    }
    node* current = list->tail;
    node* temp = NULL;
    printf("List has %zu nodes\n", list->size);
    while(current) {
        printf("%s\n", (char*)current->data);
        current = current->prev;
    }
}

//This function will delete the node whose data matches the input data, if
//found, and return true on success. Otherwise, return false.
bool
delete_node(dl_list* list, void* data) {
    if (!list || !data) {
        return false;
    }
    if (list->size == 0) {
        return false;
    }
    //Need a temp node
    node* temp = NULL;   
 
    //check the list head. Does it contain the data?
    if  (strcmp(list->head->data, data) == 0) {
        temp = list->head;
        list->head = list->head->next;
        free(temp);
        list->size--;
        return true;
    //Does the tail contain the data?
    } else if (strcmp(list->tail->data, data) == 0) {
        temp = list->tail;
        list->tail = list->tail->prev;
        list->tail->next = NULL;
        free(temp);
        list->size--;
    }

    node* current = list->head;
    while(current) {
        if (strncmp(current->data, data, strlen(current->data)) == 0) {
                temp = current;
                current->prev->next = current->next;
                current->next->prev = current->prev;
                free(temp);
                list->size--;
                return true;
        }
        current = current->next;
    }
    return false;       
}


//This function will return the address of the node, if the node's data matches
//the given input data
node*
find_node(dl_list* list, void* data) {
    if (!list || !data) {
        return NULL;
    }
    if (list->size == 0) {
        return NULL;
    }
    node* current = list->head;
    while(current) {
        if (strncmp(data, current->data, strlen(current->data)) == 0) {
            printf("FOUND THE NODE\n");
            return current;
        }
        current = current->next;
    }
    return NULL;
}


/* This function will delete all the nodes but leave the list intact */
bool 
clear_list(dl_list* list) {
    if (!list) {
        return false;
    }
    node* current = list->head;
    node* temp = NULL;
    while(current) {
        temp = current->next;
        free (current);    
        current = temp;
        list->size--;
    }
}    
    

/*This function will delete all nodes and the list itself */
void 
destroy_list(dl_list* list) {
    if (!list) {
        return;
    }
    node* current = list->head;
    node* temp = NULL;
    while(current) {
        temp = current->next;
        free (current);    
        current = temp;
        list->size--;
    }

    free(list);
    list = NULL;
}


int 
main(void) {
    int ret_val = 1;
    dl_list* list = create_list();
    if (list == NULL) {
        fprintf(stderr, "Couldn't create list.\n");
        exit(ENOMEM);
    }

    add_node(list, "Oh crap!!!!");
    add_node(list, "12345");
    add_node(list, "56789");
    add_node(list, "last in");
    add_node(list, "new last in");
    add_node(list, "newer last in");
    add_node(list, "newest last in");
    
    if (!ret_val) {
        fprintf(stderr, "Couldn't add node.\n");
        exit(ENOMEM);
    }

    //TESTS FOLLOW
    print_list_forward(list);
    printf("\n\n");
    print_list_reverse(list);

    node* node = find_node(list, "12345");
    printf("*************************************\n");
    if (node != NULL) {
        printf("%s: %s\n", (char*)node->data, (char*)node->next->data);
    } else {
        printf("Didn't find it!!!\n");
    }

    bool deleted = delete_node(list, "12345");
    if (!deleted) {
        printf("Couldn't delete node!!!\n");
    } else {
        printf("*****************************\n");
        print_list_forward(list);
        printf("\n\n\n");
        print_list_reverse(list);
    }
    destroy_list(list);

    return 0;
}
