//This is the implementation of linked list (l_list)


#include <l_list.h>

//Functions Follow

//This function creates a linked list.
l_list* create_list() {
    l_list* list = NULL;
    if ((list = calloc(1, sizeof(*list))) == NULL) {
        return NULL;
    }
    list->head = NULL;
    list->size = 0;
    return list;
}


//This function will add a node to a list.
bool
add_node(l_list* list, long int key, char* data) {
    node* node = NULL;
    if (data == NULL) {
        return false;
    }
    if ((node = calloc(1, sizeof(*node))) == NULL) {
        return false;
    }
    node->key = key;
    node->data = calloc(1, strnlen(data, MAX_STRING) + 1);
    if (node->data == NULL) {
        free(node);
        return false;
    }
    
    memcpy(node->data, data, strnlen(data, MAX_STRING));
    if (list->head == NULL) {
        list->head = node;
        list->head->next = NULL;
        list->size++;
        return true;
    }
    node->next = list->head;
    list->head = node;
    list->size++;
    return true;
}


//This function will print the data for a given list, if the list exists.
void
print_list(l_list* list) {
    if (!list) {
        fprintf(stderr, "invalid list\n");
    }
    node* current = list->head;
    node* temp = NULL;
    printf("List has %zu nodes\n", list->size);
    while(current) {
        printf("KEY: %ld\t\t\tDATA: %s\n", current->key, current->data);
        current = current->next;
    }
}


//This function will delete the node whose data matches the input data, if
//found, and return true on success. Otherwise, return false.
bool
delete_node(l_list* list, long int key) {
    if (list == NULL || key < 0) {
        return false;
    }
    if (list->size == 0) {
        return false;
    }
    //Need a temp node
    node* temp = NULL;   
 
    //check the list head. Does it contain the data?
    if (list->head->key == key) {
        free(list->head->data);
        temp = list->head;
        list->head = list->head->next;
        free(temp);
        list->size--;
        return true;
    }

    node* current = list->head;
    while(current) {
        if (current->next->key == key) {
            free(current->next->data);
            temp = current->next;
            current->next = current->next->next;
            free(temp);
            list->size--;
            return true;
        }

        current = current->next;
    }
    return false;       
}


    //This function will return the address of the node, if the node's data matches
    //the given input data
    node*
    find_node(l_list* list, long int key) {
        if (list == NULL || key < 0) {
            return NULL;
        }
        if (list->size == 0) {
            return NULL;
        }
        node* current = list->head;
        while(current) {
            if (current->key == key) {
                return current;
            }
            current = current->next;
        }
        return NULL;
    }


    /* This function will delete all the nodes but leave the list intact */
bool 
clear_list(l_list* list) {
    if (!list) {
        return false;
    }
    node* current = list->head;
    node* temp = NULL;
    while(current) {
        free(current->data);
        temp = current->next;
        free (current);    
        current = temp;
        list->size--;
    }
}    
    

/*This function will delete all nodes and the list itself,
    and free all dynamically allocated memory */
void 
destroy_list(l_list* list) {
    if (!list) {
        return;
    }
    node* current = list->head;
    node* temp = NULL;
    while(current) {
        free(current->data);
        temp = current->next;
        free (current);    
        current = temp;
        list->size--;
    }

    free(list);
}
